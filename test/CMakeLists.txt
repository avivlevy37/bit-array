cmake_minimum_required(VERSION 3.5.1)
project(ChatTests CXX)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg -ftest-coverage -fprofile-arcs --coverage -g3 -O0 -DENABLE_AUXILIARY_TRIMMING -DTESTS")
SET(LINKFLAGS "${LINKFLAGS} -lgcov --coverage")

set(CMAKE_CXX_STANDARD 17)

add_definitions("-g -Werror -Wall")

add_subdirectory(googletest)
add_subdirectory(src)