#include <cstring>
#include <iostream>

#include "gtest/gtest.h"

int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	int result = RUN_ALL_TESTS();
	std::cout << result << std::endl;
}
